![ss](https://i.imgur.com/8R6TPx0.png)
I got tired of using bookmarks and shortcuts in firefox, so I made this.
# How to Install:
* The following Instrutions are intended for Firefox/Firefox-ESR or any FF/Geko based Browser. Although you can use this on Chromium/Chromium-based and WebKit based browsers. I only use FF so I can only provide instrutions for that.
* Step One: Download/Clone this repo into your desired location.
* Step Two: Go to Settings -> Homepage and new windows
* Step Three: Add html file location. (EG:file:///home/Yotsuba/Dev/Homepage/HomePage.html)
* Step Four: Start a Communist revolution.

# Explnaition
All this really does is randomly select an image out of the 12 ones in the repo and set them as the page background then overlays some links to sites I commenly vist. I find this alot better than just using bookmarks and shortcuts.

# Credit:
Yotsuba VLC art by u/Character-Memory-248 https://www.reddit.com/r/yotsuba/comments/1cmsxgf/i_made_an_icon_to_vlc_player_%CA%9A%C9%9E/

# Lisence
* The Based GPL 3.0(or newer).